# Help Desk - Backend

Sistema de cadastro e gestão de tickets.

Frontend: [Help Desk - Front](https://bitbucket.org/programacao-iii/help-desk-front/src/master/)

<hr/>

## Informações

Backend desenvolvido em Java com Spring e JPA.

[Collection do postman](Hep Desk Api.postman_collection.json)

A api contém rotas para:
- Cadastrar um novo ticket
- Listar tickets
- Exibir detalhes de um ticket
- Atualizar um ticket

Estrutura base de um ticket:
```java
public class Ticket {

    private int ticket_id;

    private String name_issuer;

    private String email_issuer;

    private String name_responder;

    private String email_responder;

    private String title;

    private String description;

    private LocalDate open_date;

    private int status;

    private LocalDate close_date;

}
```

## Regras de negócio:

- [x] Todos os campos são obrigatórios.

- [x] Validar e-mail.

- [x] Ao cadastrar um novo ticket, o valor 1 (Aberto) deve ser atribuido ao status.

- [x] Ao cadastrar um novo ticket, a data de abertura deve ser preenchida automaticamente com a data atual.

- [x] Quando um status de ticket for atualizado para 2 (Fechado), o campo data de fechamento deve ser preenchido com a data atual.

- [x] Um ticket com o status 2 (Fechado) não poderá mais ser atualizado.