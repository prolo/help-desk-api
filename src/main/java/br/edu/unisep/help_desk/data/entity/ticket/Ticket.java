package br.edu.unisep.help_desk.data.entity.ticket;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "tickets")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticket_id")
    private int id;

    @Column(name = "name_issuer")
    private String name_issuer;

    @Column(name = "email_issuer")
    private String email_issuer;

    @Column(name = "name_responder")
    private String name_responder;

    @Column(name = "email_responder")
    private String email_responder;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "open_date")
    private LocalDate open_date;

    @Column(name = "status")
    private int status;

    @Column(name = "close_date")
    private LocalDate close_date;

}
