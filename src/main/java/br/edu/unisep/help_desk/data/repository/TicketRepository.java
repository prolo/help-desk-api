package br.edu.unisep.help_desk.data.repository;

import br.edu.unisep.help_desk.data.entity.ticket.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Integer> {

    @Query("select t from Ticket t where UPPER(t.name_issuer) like concat('%', UPPER(?1), '%') or " +
            "UPPER(t.name_responder) like concat('%', UPPER(?1),'%')")
    List<Ticket> findByNameIssuerOrNameResponder(String name);

    @Query("select t from Ticket t where UPPER(t.name_issuer) like concat('%', UPPER(?1), '%')")
    List<Ticket> findByNameIssuer(String name_issuer);

    @Query("select t from Ticket t where UPPER(t.name_responder) like concat('%', UPPER(?1), '%')")
    List<Ticket> findByNameResponder(String name_responder);

}
