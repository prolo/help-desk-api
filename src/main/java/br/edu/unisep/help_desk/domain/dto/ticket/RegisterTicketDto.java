package br.edu.unisep.help_desk.domain.dto.ticket;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterTicketDto {

    private String name_issuer;

    private String email_issuer;

    private String name_responder;

    private String email_responder;

    private String title;

    private String description;

}
