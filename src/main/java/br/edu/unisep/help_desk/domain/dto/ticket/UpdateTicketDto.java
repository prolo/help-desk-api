package br.edu.unisep.help_desk.domain.dto.ticket;

import lombok.Data;

@Data
public class UpdateTicketDto extends RegisterTicketDto {

    private Integer id;
    private Integer status;

}
