package br.edu.unisep.help_desk.domain.validator;

public class ValidationMessages {

    private ValidationMessages() {}

    public static final String MESSAGE_REQUIRED_TICKET_ID = "Informe o id do ticket";
    public static final String MESSAGE_REQUIRED_NAME_ISSUER = "Informe o nome do emissor";
    public static final String MESSAGE_REQUIRED_EMAIL_ISSUER = "Informe o email do emissor";
    public static final String MESSAGE_REQUIRED_NAME_RESPONDER = "Informe o nome do responsável";
    public static final String MESSAGE_REQUIRED_EMAIL_RESPONDER = "Informe o email do responsável";
    public static final String MESSAGE_REQUIRED_TITLE = "Informe o título do ticket";
    public static final String MESSAGE_REQUIRED_DESCRIPTION = "Informe a descrição do ticket";
    public static final String MESSAGE_REQUIRED_STATUS = "Informe o status do ticket";

    public static final String MESSAGE_INVALID_EMAIL_ISSUER = "Email do emissor é inválido";
    public static final String MESSAGE_INVALID_EMAIL_RESPONDER = "Email do responsável é inválido";

    public static final String MESSAGE_CANNOT_UPDATE_CLOSED_TICKET = "Não é possível alterar um ticket fechado";
}
