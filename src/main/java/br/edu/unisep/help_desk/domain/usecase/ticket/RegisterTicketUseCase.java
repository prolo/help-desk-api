package br.edu.unisep.help_desk.domain.usecase.ticket;

import br.edu.unisep.help_desk.data.entity.ticket.Ticket;
import br.edu.unisep.help_desk.data.repository.TicketRepository;
import br.edu.unisep.help_desk.domain.builder.ticket.TicketBuilder;
import br.edu.unisep.help_desk.domain.dto.ticket.RegisterTicketDto;
import br.edu.unisep.help_desk.domain.validator.ticket.RegisterTicketValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterTicketUseCase {

    private final RegisterTicketValidator validator;
    private final TicketBuilder builder;
    private final TicketRepository ticketRepository;

    public Ticket execute(RegisterTicketDto registerTicket) {
        validator.validate(registerTicket);

        var ticket = builder.from(registerTicket);
        return ticketRepository.save(ticket);
    }
}
