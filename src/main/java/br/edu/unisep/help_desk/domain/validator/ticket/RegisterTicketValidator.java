package br.edu.unisep.help_desk.domain.validator.ticket;

import br.edu.unisep.help_desk.domain.dto.ticket.RegisterTicketDto;
import org.apache.commons.lang3.Validate;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;

import static br.edu.unisep.help_desk.domain.validator.ValidationMessages.*;

@Component
public class RegisterTicketValidator {

    public void validate(RegisterTicketDto ticket) {
        Validate.notBlank(ticket.getName_issuer(), MESSAGE_REQUIRED_NAME_ISSUER);
        Validate.notBlank(ticket.getEmail_issuer(), MESSAGE_REQUIRED_EMAIL_ISSUER);
        Validate.notBlank(ticket.getName_responder(), MESSAGE_REQUIRED_NAME_RESPONDER);
        Validate.notBlank(ticket.getEmail_responder(), MESSAGE_REQUIRED_EMAIL_RESPONDER);
        Validate.notBlank(ticket.getTitle(), MESSAGE_REQUIRED_TITLE);
        Validate.notBlank(ticket.getDescription(), MESSAGE_REQUIRED_DESCRIPTION);

        var emailValidator = EmailValidator.getInstance();

        Validate.isTrue(emailValidator.isValid(ticket.getEmail_issuer()), MESSAGE_INVALID_EMAIL_ISSUER);
        Validate.isTrue(emailValidator.isValid(ticket.getEmail_responder()), MESSAGE_INVALID_EMAIL_RESPONDER);
    }
}
