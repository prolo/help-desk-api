package br.edu.unisep.help_desk.domain.dto.ticket;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class TicketDto {

    private int id;

    private String name_issuer;

    private String email_issuer;

    private String name_responder;

    private String email_responder;

    private String title;

    private String description;

    private LocalDate open_date;

    private int status;

    private LocalDate close_date;

}
