package br.edu.unisep.help_desk.domain.usecase.ticket;

import br.edu.unisep.help_desk.data.repository.TicketRepository;
import br.edu.unisep.help_desk.domain.builder.ticket.TicketBuilder;
import br.edu.unisep.help_desk.domain.dto.ticket.TicketDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindAllTicketsUseCase {

    private final TicketRepository ticketRepository;
    private final TicketBuilder builder;

    public List<TicketDto> execute() {
        var ticket = ticketRepository.findAll();
        return builder.from(ticket);
    }
}
