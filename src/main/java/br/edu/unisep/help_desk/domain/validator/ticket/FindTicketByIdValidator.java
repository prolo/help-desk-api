package br.edu.unisep.help_desk.domain.validator.ticket;

import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.help_desk.domain.validator.ValidationMessages.MESSAGE_REQUIRED_TICKET_ID;

@Component
public class FindTicketByIdValidator {

    public void validate(Integer id) {
        Validate.isTrue(id > 0, MESSAGE_REQUIRED_TICKET_ID);
    }
}
