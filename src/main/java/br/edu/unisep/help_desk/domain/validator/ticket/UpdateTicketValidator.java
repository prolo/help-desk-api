package br.edu.unisep.help_desk.domain.validator.ticket;

import br.edu.unisep.help_desk.domain.dto.ticket.UpdateTicketDto;
import br.edu.unisep.help_desk.domain.usecase.ticket.FindTicketByIdUseCase;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.help_desk.domain.validator.ValidationMessages.*;

@Component
@AllArgsConstructor
public class UpdateTicketValidator {

    private final RegisterTicketValidator registerValidator;
    private final FindTicketByIdUseCase findTicket;

    public void validate(UpdateTicketDto ticket) {
        Validate.notNull(ticket.getId(), MESSAGE_REQUIRED_TICKET_ID);
        Validate.isTrue(ticket.getId() > 0, MESSAGE_REQUIRED_TICKET_ID);

        Validate.notNull(ticket.getStatus(), MESSAGE_REQUIRED_STATUS);

        registerValidator.validate(ticket);

        validateClosedTicket(ticket.getId());
    }

    private void validateClosedTicket(Integer id) {
        var ticket = findTicket.execute(id);

        Validate.isTrue(ticket.getStatus() != 2, MESSAGE_CANNOT_UPDATE_CLOSED_TICKET);
    }
}
