package br.edu.unisep.help_desk.domain.builder.ticket;

import br.edu.unisep.help_desk.data.entity.ticket.Ticket;
import br.edu.unisep.help_desk.domain.dto.ticket.RegisterTicketDto;
import br.edu.unisep.help_desk.domain.dto.ticket.TicketDto;
import br.edu.unisep.help_desk.domain.dto.ticket.UpdateTicketDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TicketBuilder {

    public List<TicketDto> from(List<Ticket> tickets) {
        return tickets.stream().map(this::from).collect(Collectors.toList());
    }

    public TicketDto from(Ticket ticket) {
        return new TicketDto(
                ticket.getId(),
                ticket.getName_issuer(),
                ticket.getEmail_issuer(),
                ticket.getName_responder(),
                ticket.getEmail_responder(),
                ticket.getTitle(),
                ticket.getDescription(),
                ticket.getOpen_date(),
                ticket.getStatus(),
                ticket.getClose_date()
        );
    }

    public Ticket from(RegisterTicketDto registerTicketDto) {
        Ticket ticket = new Ticket();
        ticket.setName_issuer(registerTicketDto.getName_issuer());
        ticket.setEmail_issuer(registerTicketDto.getEmail_issuer());
        ticket.setName_responder(registerTicketDto.getName_responder());
        ticket.setEmail_responder(registerTicketDto.getEmail_responder());
        ticket.setTitle(registerTicketDto.getTitle());
        ticket.setDescription(registerTicketDto.getDescription());
        ticket.setOpen_date(LocalDate.now());
        ticket.setStatus(0);

        return ticket;
    }

    public Ticket from(UpdateTicketDto updateTicket) {
        Ticket ticket = from((RegisterTicketDto) updateTicket);
        ticket.setId(updateTicket.getId());
        ticket.setStatus(updateTicket.getStatus());

        if (ticket.getStatus() == 2) {
            ticket.setClose_date(LocalDate.now());
        }

        return ticket;
    }
}
