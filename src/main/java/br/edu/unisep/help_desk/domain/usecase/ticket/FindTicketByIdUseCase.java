package br.edu.unisep.help_desk.domain.usecase.ticket;

import br.edu.unisep.help_desk.data.repository.TicketRepository;
import br.edu.unisep.help_desk.domain.builder.ticket.TicketBuilder;
import br.edu.unisep.help_desk.domain.dto.ticket.TicketDto;
import br.edu.unisep.help_desk.domain.validator.ticket.FindTicketByIdValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FindTicketByIdUseCase {

    private final FindTicketByIdValidator validator;
    private final TicketRepository ticketRepository;
    private final TicketBuilder builder;

    public TicketDto execute(Integer ticketId) {
        validator.validate(ticketId);

        var ticket = ticketRepository.findById(ticketId);
        return ticket.map(builder::from).orElse(null);
    }
}
