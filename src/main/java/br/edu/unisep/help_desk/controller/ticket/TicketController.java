package br.edu.unisep.help_desk.controller.ticket;
import br.edu.unisep.help_desk.data.entity.ticket.Ticket;
import br.edu.unisep.help_desk.domain.dto.ticket.RegisterTicketDto;
import br.edu.unisep.help_desk.domain.dto.ticket.TicketDto;
import br.edu.unisep.help_desk.domain.dto.ticket.UpdateTicketDto;
import br.edu.unisep.help_desk.domain.usecase.ticket.*;
import br.edu.unisep.help_desk.response.DefaultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/ticket")
public class TicketController {

    private final FindAllTicketsUseCase findAllTickets;
    private final FindTicketByIdUseCase findById;
    private final RegisterTicketUseCase registerTicket;
    private final UpdateTicketUseCase updateTicket;
    private final FindTicketByNameIssuerOrNameResponderUseCase findByNameIssuerOrNameResponder;
    private final FindTicketByNameIssuerUseCase findByNameIssuer;
    private final FindTicketByNameResponderUseCase findByNameResponder;

    @GetMapping
    public ResponseEntity<DefaultResponse<List<TicketDto>>> findAll(){

        var tickets = findAllTickets.execute();

        return tickets.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(DefaultResponse.of(tickets));
    }

    @GetMapping("/{id}")
    public ResponseEntity<DefaultResponse<TicketDto>> findById(@PathVariable("id") Integer id){

        var ticket = findById.execute(id);

        return ticket == null ? ResponseEntity.notFound().build() : ResponseEntity.ok(DefaultResponse.of(ticket));
    }

    @PostMapping
    public ResponseEntity<DefaultResponse<Ticket>> save(@RequestBody RegisterTicketDto ticket){

        var result = registerTicket.execute(ticket);

        return ResponseEntity.ok(DefaultResponse.of(result));
    }

    @PutMapping
    public ResponseEntity<DefaultResponse<Boolean>> update(@RequestBody UpdateTicketDto ticket){

        updateTicket.execute(ticket);

        return ResponseEntity.ok(DefaultResponse.of(true));
    }

    @GetMapping("/byNameIssuerOrNameResponder")
    public ResponseEntity<DefaultResponse<List<TicketDto>>> findByNameIssuerOrNameResponder(@RequestParam("name") String name) {
        var tickets = findByNameIssuerOrNameResponder.execute(name);

        return tickets.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(DefaultResponse.of(tickets));
    }

    @GetMapping("/byNameIssuer")
    public ResponseEntity<DefaultResponse<List<TicketDto>>> findByNameIssuer(@RequestParam("name") String name_issuer) {
        var tickets = findByNameIssuer.execute(name_issuer);

        return tickets.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(DefaultResponse.of(tickets));
    }

    @GetMapping("/byNameResponder")
    public ResponseEntity<DefaultResponse<List<TicketDto>>> findByNameResponder(@RequestParam("name") String name_responder) {
        var tickets = findByNameResponder.execute(name_responder);

        return tickets.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(DefaultResponse.of(tickets));
    }
}
